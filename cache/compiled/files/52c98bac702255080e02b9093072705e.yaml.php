<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://my-demo-theme/my-demo-theme.yaml',
    'modified' => 1520350162,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => true
        ]
    ]
];
